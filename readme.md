## Getting started

`composer install`

`mkdir -p config/jwt`

`openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096`

`openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout`

Passphrase, который нужен для генерации ключей, находится в `.env` файле в поле `JWT_PASSPHRASE`: `e32ea6dfe4e7960b7d72d1e7c79fe157`

В `.env` файле надо изменить доступ к БД.

Развернуть БД (выполнить все миграции): `php bin/console doctrine:migrations:migrate`