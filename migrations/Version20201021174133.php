<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201021174133 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE crop_rotation (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, field_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, year INT NOT NULL, season INT NOT NULL, crop_sort INT DEFAULT NULL, date_seeding DATETIME DEFAULT NULL, date_harvest DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_A0ECC120C54C8C93 (type_id), INDEX IDX_A0ECC120443707B0 (field_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE crop_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE field (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, district VARCHAR(255) NOT NULL, field_shape INT NOT NULL, field_area DOUBLE PRECISION NOT NULL, state VARCHAR(255) DEFAULT NULL, territory VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_5BF54558A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, field_id INT DEFAULT NULL, research_id INT DEFAULT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, path VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_14B78418C54C8C93 (type_id), INDEX IDX_14B78418443707B0 (field_id), INDEX IDX_14B784187909E1ED (research_id), INDEX IDX_14B78418A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE refresh_tokens (id INT AUTO_INCREMENT NOT NULL, refresh_token VARCHAR(128) NOT NULL, username VARCHAR(255) NOT NULL, valid DATETIME NOT NULL, UNIQUE INDEX UNIQ_9BACE7E1C74F2195 (refresh_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE research (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, user_id INT NOT NULL, poi_name VARCHAR(255) NOT NULL, poi_latitude DOUBLE PRECISION NOT NULL, poi_longitude DOUBLE PRECISION NOT NULL, comment VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_57EB50C2C54C8C93 (type_id), INDEX IDX_57EB50C2A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE research_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, login VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_8D93D649AA08CB10 (login), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE crop_rotation ADD CONSTRAINT FK_A0ECC120C54C8C93 FOREIGN KEY (type_id) REFERENCES crop_type (id)');
        $this->addSql('ALTER TABLE crop_rotation ADD CONSTRAINT FK_A0ECC120443707B0 FOREIGN KEY (field_id) REFERENCES field (id)');
        $this->addSql('ALTER TABLE field ADD CONSTRAINT FK_5BF54558A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE photo ADD CONSTRAINT FK_14B78418C54C8C93 FOREIGN KEY (type_id) REFERENCES photo_type (id)');
        $this->addSql('ALTER TABLE photo ADD CONSTRAINT FK_14B78418443707B0 FOREIGN KEY (field_id) REFERENCES field (id)');
        $this->addSql('ALTER TABLE photo ADD CONSTRAINT FK_14B784187909E1ED FOREIGN KEY (research_id) REFERENCES research (id)');
        $this->addSql('ALTER TABLE photo ADD CONSTRAINT FK_14B78418A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE research ADD CONSTRAINT FK_57EB50C2C54C8C93 FOREIGN KEY (type_id) REFERENCES research_type (id)');
        $this->addSql('ALTER TABLE research ADD CONSTRAINT FK_57EB50C2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('INSERT INTO research_type VALUES (DEFAULT, "Добавление POI с комментарием", "' . date('c') . '"), (DEFAULT, "Измерение высоты растений", "' . date('c') . '"), (DEFAULT, "Фиксация пораженных растений", "' . date('c') . '")');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE crop_rotation DROP FOREIGN KEY FK_A0ECC120C54C8C93');
        $this->addSql('ALTER TABLE crop_rotation DROP FOREIGN KEY FK_A0ECC120443707B0');
        $this->addSql('ALTER TABLE photo DROP FOREIGN KEY FK_14B78418443707B0');
        $this->addSql('ALTER TABLE photo DROP FOREIGN KEY FK_14B78418C54C8C93');
        $this->addSql('ALTER TABLE photo DROP FOREIGN KEY FK_14B784187909E1ED');
        $this->addSql('ALTER TABLE research DROP FOREIGN KEY FK_57EB50C2C54C8C93');
        $this->addSql('ALTER TABLE field DROP FOREIGN KEY FK_5BF54558A76ED395');
        $this->addSql('ALTER TABLE photo DROP FOREIGN KEY FK_14B78418A76ED395');
        $this->addSql('ALTER TABLE research DROP FOREIGN KEY FK_57EB50C2A76ED395');
        $this->addSql('DROP TABLE crop_rotation');
        $this->addSql('DROP TABLE crop_type');
        $this->addSql('DROP TABLE field');
        $this->addSql('DROP TABLE photo');
        $this->addSql('DROP TABLE photo_type');
        $this->addSql('DROP TABLE refresh_tokens');
        $this->addSql('DROP TABLE research');
        $this->addSql('DROP TABLE research_type');
        $this->addSql('DROP TABLE user');
    }
}
