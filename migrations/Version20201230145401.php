<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201230145401 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE field ADD grampancyat VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE research ADD poiaccuracy DOUBLE PRECISION DEFAULT NULL, ADD poiauto_latitude DOUBLE PRECISION DEFAULT NULL, ADD poiauto_longitude DOUBLE PRECISION DEFAULT NULL, ADD poiautoaccuracy DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE field DROP grampancyat');
        $this->addSql('ALTER TABLE research DROP poiaccuracy, DROP poiauto_latitude, DROP poiauto_longitude, DROP poiautoaccuracy');
    }
}
