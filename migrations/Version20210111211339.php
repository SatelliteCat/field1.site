<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210111211339 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE crop_rotation ADD fym_used TINYINT(1) DEFAULT NULL, ADD fym_quality VARCHAR(255) DEFAULT NULL, ADD chemical_fertilizer_quality VARCHAR(255) DEFAULT NULL, ADD chemical_fertilizer_used TINYINT(1) DEFAULT NULL, CHANGE name name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE field ADD kml VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE `field1`.`crop_rotation` MODIFY COLUMN `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL AFTER `field_id`;');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE crop_rotation DROP fym_used, DROP fym_quality, DROP chemical_fertilizer_quality, DROP chemical_fertilizer_used, CHANGE name name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE field DROP kml');
        $this->addSql('ALTER TABLE `field1`.`crop_rotation` MODIFY COLUMN `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL AFTER `field_id`;');
    }
}
