<?php

namespace App\Repository;

use App\Entity\CropRotation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CropRotation|null find($id, $lockMode = null, $lockVersion = null)
 * @method CropRotation|null findOneBy(array $criteria, array $orderBy = null)
 * @method CropRotation[]    findAll()
 * @method CropRotation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CropRotationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CropRotation::class);
    }

    public function save(CropRotation ...$model): void
    {
        foreach ($model as $item) {
            $this->_em->persist($item);
        }

        $this->_em->flush();
    }

    public function exists($id): bool
    {
        return $this->count(['id' => $id]) > 0;
    }

    // /**
    //  * @return CropRotation[] Returns an array of CropRotation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CropRotation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
