<?php

namespace App\Repository;

use App\Entity\ResearchType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ResearchType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResearchType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResearchType[]    findAll()
 * @method ResearchType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResearchTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResearchType::class);
    }

    public function exists($id): bool
    {
        return $this->count(['id' => $id]) > 0;
    }

    // /**
    //  * @return ResearchType[] Returns an array of ResearchType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ResearchType
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
