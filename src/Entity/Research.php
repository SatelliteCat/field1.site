<?php

namespace App\Entity;

use App\Repository\ResearchRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ResearchRepository::class)
 */
class Research
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $poiName;

    /**
     * @ORM\Column(type="float")
     */
    private $poiLatitude;

    /**
     * @ORM\Column(type="float")
     */
    private $poiLongitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity=ResearchType::class, inversedBy="researches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="researches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Photo::class, mappedBy="research")
     */
    private $photos;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $poiaccuracy;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $poiautoLatitude;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $poiautoLongitude;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $poiautoaccuracy;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPoiName(): ?string
    {
        return $this->poiName;
    }

    public function setPoiName(string $poiName): self
    {
        $this->poiName = $poiName;

        return $this;
    }

    public function getPoiLatitude(): ?float
    {
        return $this->poiLatitude;
    }

    public function setPoiLatitude(float $poiLatitude): self
    {
        $this->poiLatitude = $poiLatitude;

        return $this;
    }

    public function getPoiLongitude(): ?float
    {
        return $this->poiLongitude;
    }

    public function setPoiLongitude(float $poiLongitude): self
    {
        $this->poiLongitude = $poiLongitude;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getType(): ?ResearchType
    {
        return $this->type;
    }

    public function setType(?ResearchType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->setResearch($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
            // set the owning side to null (unless already changed)
            if ($photo->getResearch() === $this) {
                $photo->setResearch(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPoiaccuracy(): ?float
    {
        return $this->poiaccuracy;
    }

    public function setPoiaccuracy(?float $poiaccuracy): self
    {
        $this->poiaccuracy = $poiaccuracy;

        return $this;
    }

    public function getPoiautoLatitude(): ?float
    {
        return $this->poiautoLatitude;
    }

    public function setPoiautoLatitude(?float $poiautoLatitude): self
    {
        $this->poiautoLatitude = $poiautoLatitude;

        return $this;
    }

    public function getPoiautoLongitude(): ?float
    {
        return $this->poiautoLongitude;
    }

    public function setPoiautoLongitude(?float $poiautoLongitude): self
    {
        $this->poiautoLongitude = $poiautoLongitude;

        return $this;
    }

    public function getPoiautoaccuracy(): ?float
    {
        return $this->poiautoaccuracy;
    }

    public function setPoiautoaccuracy(?float $poiautoaccuracy): self
    {
        $this->poiautoaccuracy = $poiautoaccuracy;

        return $this;
    }
}
