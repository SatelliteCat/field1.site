<?php

namespace App\Entity;

use App\Repository\CropRotationRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CropRotationRepository::class)
 */
class CropRotation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @ORM\Column(type="integer")
     */
    private $season;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cropSort;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateSeeding;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateHarvest;

    /**
     * @ORM\ManyToOne(targetEntity=CropType::class, inversedBy="cropRotations")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=Field::class, inversedBy="cropRotations")
     */
    private $field;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fymUsed;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fymQuality;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $chemicalFertilizerQuality;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $chemicalFertilizerUsed;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return (string)$this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getSeason(): ?int
    {
        return $this->season;
    }

    public function setSeason(int $season): self
    {
        $this->season = $season;

        return $this;
    }

    public function getCropSort(): ?int
    {
        return $this->cropSort;
    }

    public function setCropSort(?int $cropSort): self
    {
        $this->cropSort = $cropSort;

        return $this;
    }

    public function getDateSeeding(): ?DateTimeInterface
    {
        return $this->dateSeeding;
    }

    public function setDateSeeding(?DateTimeInterface $dateSeeding): self
    {
        $this->dateSeeding = $dateSeeding;

        return $this;
    }

    public function getDateHarvest(): ?DateTimeInterface
    {
        return $this->dateHarvest;
    }

    public function setDateHarvest(?DateTimeInterface $dateHarvest): self
    {
        $this->dateHarvest = $dateHarvest;

        return $this;
    }

    public function getType(): ?CropType
    {
        return $this->type;
    }

    public function setType(?CropType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getField(): ?Field
    {
        return $this->field;
    }

    public function setField(?Field $field): self
    {
        $this->field = $field;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getFymUsed(): ?bool
    {
        return $this->fymUsed;
    }

    public function setFymUsed(?bool $fymUsed): self
    {
        $this->fymUsed = $fymUsed;

        return $this;
    }

    public function getFymQuality(): ?string
    {
        return $this->fymQuality;
    }

    public function setFymQuality(?string $fymQuality): self
    {
        $this->fymQuality = $fymQuality;

        return $this;
    }

    public function getChemicalFertilizerQuality(): ?string
    {
        return $this->chemicalFertilizerQuality;
    }

    public function setChemicalFertilizerQuality(?string $chemicalFertilizerQuality): self
    {
        $this->chemicalFertilizerQuality = $chemicalFertilizerQuality;

        return $this;
    }

    public function getChemicalFertilizerUsed(): ?bool
    {
        return $this->chemicalFertilizerUsed;
    }

    public function setChemicalFertilizerUsed(?bool $chemicalFertilizerUsed): self
    {
        $this->chemicalFertilizerUsed = $chemicalFertilizerUsed;

        return $this;
    }
}
