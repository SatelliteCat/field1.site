<?php

namespace App\Entity;

use App\Repository\FieldRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FieldRepository::class)
 */
class Field
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $district;

    /**
     * @ORM\Column(type="integer")
     */
    private $fieldShape;

    /**
     * @ORM\Column(type="float")
     */
    private $fieldArea;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $territory;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="fields")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Photo::class, mappedBy="field")
     */
    private $photos;

    /**
     * @ORM\OneToMany(targetEntity=CropRotation::class, mappedBy="field")
     */
    private $cropRotations;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $grampancyat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $kml;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
        $this->cropRotations = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return (string)$this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDistrict(): string
    {
        return (string)$this->district;
    }

    public function setDistrict(string $district): self
    {
        $this->district = $district;

        return $this;
    }

    public function getFieldShape(): int
    {
        return (int)$this->fieldShape;
    }

    public function setFieldShape(int $fieldShape): self
    {
        $this->fieldShape = $fieldShape;

        return $this;
    }

    public function getFieldArea(): float
    {
        return (float)$this->fieldArea;
    }

    public function setFieldArea(float $fieldArea): self
    {
        $this->fieldArea = $fieldArea;

        return $this;
    }

    public function getState(): string
    {
        return (string)$this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getTerritory(): string
    {
        return (string)$this->territory;
    }

    public function setTerritory(?string $territory): self
    {
        $this->territory = $territory;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->setField($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
            // set the owning side to null (unless already changed)
            if ($photo->getField() === $this) {
                $photo->setField(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CropRotation[]
     */
    public function getCropRotations(): Collection
    {
        return $this->cropRotations;
    }

    public function addCropRotation(CropRotation $cropRotation): self
    {
        if (!$this->cropRotations->contains($cropRotation)) {
            $this->cropRotations[] = $cropRotation;
            $cropRotation->setField($this);
        }

        return $this;
    }

    public function removeCropRotation(CropRotation $cropRotation): self
    {
        if ($this->cropRotations->contains($cropRotation)) {
            $this->cropRotations->removeElement($cropRotation);
            // set the owning side to null (unless already changed)
            if ($cropRotation->getField() === $this) {
                $cropRotation->setField(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getGrampancyat(): string
    {
        return (string)$this->grampancyat;
    }

    public function setGrampancyat(?string $grampancyat): self
    {
        $this->grampancyat = $grampancyat;

        return $this;
    }

    public function getKml(): string
    {
        return (string)$this->kml;
    }

    public function setKml(?string $kml): self
    {
        $this->kml = $kml;

        return $this;
    }
}
