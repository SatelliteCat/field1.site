<?php

namespace App\Entity;

use App\Repository\CropTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CropTypeRepository::class)
 */
class CropType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=CropRotation::class, mappedBy="type")
     */
    private $cropRotations;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->cropRotations = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|CropRotation[]
     */
    public function getCropRotations(): Collection
    {
        return $this->cropRotations;
    }

    public function addCropRotation(CropRotation $cropRotation): self
    {
        if (!$this->cropRotations->contains($cropRotation)) {
            $this->cropRotations[] = $cropRotation;
            $cropRotation->setType($this);
        }

        return $this;
    }

    public function removeCropRotation(CropRotation $cropRotation): self
    {
        if ($this->cropRotations->contains($cropRotation)) {
            $this->cropRotations->removeElement($cropRotation);
            // set the owning side to null (unless already changed)
            if ($cropRotation->getType() === $this) {
                $cropRotation->setType(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
