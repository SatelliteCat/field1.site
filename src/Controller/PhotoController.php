<?php


namespace App\Controller;

use App\Entity\Photo;
use App\Repository\FieldRepository;
use App\Repository\PhotoRepository;
use App\Repository\PhotoTypeRepository;
use App\Repository\ResearchRepository;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class FileController
 * @package App\Controller
 * @Route("/api/photo", name="file_api")
 */
class PhotoController extends ApiController
{
    private $photoRepository;
    private $photoTypeRepository;
    private $fieldRepository;
    private $researchRepository;

    public function __construct(PhotoRepository $photoRepository, PhotoTypeRepository $photoTypeRepository, FieldRepository $fieldRepository, ResearchRepository $researchRepository)
    {
        $this->photoRepository = $photoRepository;
        $this->photoTypeRepository = $photoTypeRepository;
        $this->fieldRepository = $fieldRepository;
        $this->researchRepository = $researchRepository;
    }

    /**
     * @Route("/upload", name="app_product_new")
     * @param Request $request
     * @param FileUploader $fileUploader
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    public function new(Request $request, FileUploader $fileUploader, ValidatorInterface $validator): JsonResponse
    {
        $file = $request->files->get('photo');
        $params = $this->transformJsonBody($request)->request->all();

        $isCorrectParams = isset($params['type']) && (isset($params['field']) || isset($params['research']));

        if (!$isCorrectParams) {
            return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params], '"type" is required, also must be send "field" or "research"');
        }

        if (!$this->photoTypeRepository->exists($params['type'])) {
            return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params], 'Type not found');
        }

        if (isset($params['field']) && !$this->fieldRepository->exists($params['field'])) {
            return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params], 'Field not found');
        }

        if (isset($params['research']) && !$this->researchRepository->exists($params['research'])) {
            return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params], 'Research not found');
        }

        $fileConstraint = [
//            new Assert\File([
//                'mimeTypes' => 'image/*',
//                'mimeTypesMessage' => 'Upload a valid image'
//            ]),
            new Assert\NotBlank(['message' => '"photo" is required']),
        ];

        $errors = $validator->validate(
            $file,
            $fileConstraint
        );

        if (count($errors) > 0) {
            return $this->response(Response::HTTP_BAD_REQUEST, [], $errors[0]->getMessage());
        }

        $fileName = $fileUploader->upload($file);
        if (!$fileName) {
            return $this->response(Response::HTTP_INTERNAL_SERVER_ERROR, [], 'Photo not uploaded');
        }

        try {
            $photo = new Photo();

            $photo
                ->setName($fileName)
                ->setPath($this->getParameter('photos_directory'))
                ->setType($this->photoTypeRepository->find(1))
                ->setUser($this->getApiUser());

            if (isset($params['field'])) {
                $photo->setField($this->fieldRepository->find($params['field']));
            }

            if (isset($params['research'])) {
                $photo->setResearch($this->researchRepository->find($params['research']));
            }

            $this->photoRepository->save($photo);

        } catch (\Exception $exception) {
            return $this->response(Response::HTTP_INTERNAL_SERVER_ERROR, [], 'Photo cannot create');
        }

        return $this->response(Response::HTTP_CREATED, ['id' => $photo->getId()], 'File successfully uploaded.');
    }

    /**
     * @Route("/{id}", name="get_one_photo", methods={"GET"})
     * @param $id
     * @return JsonResponse
     */
    public function getOne($id): JsonResponse
    {
        $photo = $this->photoRepository->find($id);

        if ($photo === null) {
            return $this->response(Response::HTTP_BAD_REQUEST, [], 'Photo not found.');
        }

        $result = [
            'id' => $photo->getId(),
            'name' => $photo->getName(),
            'path' => $photo->getPath(),
            'type' => $photo->getType() ? $photo->getType()->getId() : 0,
            'field' => $photo->getField() ? $photo->getField()->getId() : 0,
            'research' => $photo->getResearch() ? $photo->getResearch()->getId() : 0,
            'user' => $photo->getUser() ? $photo->getUser()->getId() : 0,
        ];

        return $this->response(Response::HTTP_OK, $result);
    }

    /**
     * @Route("/", name="get_all_photo", methods={"GET"})
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $result = array_map(static function (Photo $photo) {
            return [
                'id' => $photo->getId(),
                'name' => $photo->getName(),
                'path' => $photo->getPath(),
            ];
        }, $this->photoRepository->findAll());

        return $this->response(Response::HTTP_OK, $result);
    }

    /**
     * @Route("/user/{id}", name="get_all_photo_by_user", methods={"GET"})
     * @param $id
     * @return JsonResponse
     */
    public function getAllByUser($id): JsonResponse
    {
        $result = array_map(static function (Photo $photo) {
            return [
                'id' => $photo->getId(),
                'name' => $photo->getName(),
                'path' => $photo->getPath(),
            ];
        }, $this->photoRepository->findBy(['user' => $id]));

        return $this->response(Response::HTTP_OK, $result);
    }
}