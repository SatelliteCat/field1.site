<?php

namespace App\Controller;

use App\Entity\Research;
use App\Repository\ResearchRepository;
use App\Repository\ResearchTypeRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ResearchController
 * @package App\Controller
 * @Route("/api/research")
 */
class ResearchController extends ApiController
{
    private $researchRepository;
    private $researchTypeRepository;

    public function __construct(ResearchRepository $researchRepository, ResearchTypeRepository $researchTypeRepository)
    {
        $this->researchRepository = $researchRepository;
        $this->researchTypeRepository = $researchTypeRepository;
    }

    /**
     * @Route("/create", name="create_research", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $params = $this->transformJsonBody($request)->request->all();
        $isCorrectParams = isset(
                $params['type'],
                $params['poiName'],
                $params['poiLatitude'],
                $params['poiLongitude']
            )
            && $this->researchTypeRepository->exists($params['type'])
            && !empty($params['poiName'])
            && $params['poiLatitude'] !== ''
            && $params['poiLongitude'] !== '';

        if (!$isCorrectParams) {
            return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params]);
        }

        $research = new Research();
        $research
            ->setType($this->researchTypeRepository->find($params['type']))
            ->setPoiName($params['poiName'])
            ->setPoiLatitude((float)$params['poiLatitude'])
            ->setPoiLongitude((float)$params['poiLongitude'])
            ->setComment($params['comment'] ?? null)
            ->setPoiaccuracy($params['poiaccuracy'] ?? null)
            ->setPoiautoLatitude($params['poiautoLatitude'] ?? null)
            ->setPoiautoLongitude($params['poiautoLongitude'] ?? null)
            ->setPoiautoaccuracy($params['poiautoaccuracy'] ?? null)
            ->setUser($this->getApiUser());

        $this->researchRepository->save($research);

        return $this->response(Response::HTTP_CREATED, ['id' => $research->getId()], 'Research successfully created.');
    }

    /**
     * @Route("/update/{id}", name="update_research", methods={"POST"})
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $params = $this->transformJsonBody($request)->request->all();
        $isNotCorrectParams = (isset($params['type']) && empty($params['type'])) ||
            (isset($params['poiName']) && empty($params['poiName'])) ||
            (isset($params['poiLatitude']) && $params['poiLatitude'] === '') ||
            (isset($params['poiLongitude']) && $params['poiLongitude'] === '');

        if ($isNotCorrectParams || !$research = $this->researchRepository->find($id)) {
            return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params]);
        }

        isset($params['type']) ? $research->setType($this->researchTypeRepository->find($params['type'])) : null;
        isset($params['poiName']) ? $research->setPoiName($params['poiName']) : null;
        isset($params['poiLatitude']) ? $research->setPoiLatitude((float)$params['poiLatitude']) : null;
        isset($params['poiLongitude']) ? $research->setPoiLongitude((float)$params['poiLongitude']) : null;
        isset($params['comment']) ? $research->setComment($params['comment']) : null;
        isset($params['poiaccuracy']) ? $research->setPoiaccuracy($params['poiaccuracy']) : null;
        isset($params['poiautoLatitude']) ? $research->setPoiautoLatitude($params['poiautoLatitude']) : null;
        isset($params['poiautoLongitude']) ? $research->setPoiautoLongitude($params['poiautoLongitude']) : null;
        isset($params['poiautoaccuracy']) ? $research->setPoiautoaccuracy($params['poiautoaccuracy']) : null;

        $this->researchRepository->save($research);

        return $this->response(Response::HTTP_OK, ['id' => $research->getId()], 'Research successfully updated.');
    }

    /**
     * @Route("/", name="get_all_research", methods={"GET"})
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $result = array_map(static function (Research $research) {
            return [
                'id' => $research->getId(),
                'poiName' => $research->getPoiName(),
                'poiLatitude' => $research->getPoiLatitude(),
                'poiLongitude' => $research->getPoiLongitude(),
            ];
        }, $this->researchRepository->findAll());

        return $this->response(Response::HTTP_OK, $result);
    }

    /**
     * @Route("/{id}", name="get_one_research", methods={"GET"})
     * @param $id
     * @return JsonResponse
     */
    public function getOne($id): JsonResponse
    {
        $research = $this->researchRepository->find($id);

        if ($research === null) {
            return $this->response(Response::HTTP_BAD_REQUEST, [], 'Research not found.');
        }

        $result = [
            'id' => $research->getId(),
            'poiName' => $research->getPoiName(),
            'poiLatitude' => $research->getPoiLatitude(),
            'poiLongitude' => $research->getPoiLongitude(),
            'poiaccuracy' => $research->getPoiaccuracy(),
            'comment' => $research->getComment(),
            'type' => $research->getType() ? $research->getType()->getId() : null,
            'user' => $research->getUser() ? $research->getUser()->getId() : null,
            'createdAt' => $research->getCreatedAt()->format('Y.m.d h:i:s'),
            'poiautoLatitude' => $research->getPoiautoLatitude(),
            'poiautoLongitude' => $research->getPoiautoLongitude(),
            'poiautoaccuracy' => $research->getPoiautoaccuracy(),
        ];

        return $this->response(Response::HTTP_OK, $result);
    }
}
