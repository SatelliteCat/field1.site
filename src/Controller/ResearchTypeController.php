<?php

namespace App\Controller;

use App\Entity\ResearchType;
use App\Repository\ResearchTypeRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ResearchTypeController
 * @package App\Controller
 * @Route("/api/research-type")
 */
class ResearchTypeController extends ApiController
{
    private $researchTypeRepository;

    public function __construct(ResearchTypeRepository $researchTypeRepository)
    {
        $this->researchTypeRepository = $researchTypeRepository;
    }

    /**
     * @Route("/", name="get_all_research_type", methods={"GET"})
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $result = array_map(static function (ResearchType $researchType) {
            return [
                'id' => $researchType->getId(),
                'name' => $researchType->getName(),
            ];
        }, $this->researchTypeRepository->findAll());

        return $this->response(Response::HTTP_OK, $result);
    }
}
