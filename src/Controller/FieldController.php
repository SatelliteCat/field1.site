<?php

namespace App\Controller;

use App\Entity\Field;
use App\Repository\FieldRepository;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FieldController
 * @package App\Controller
 * @Route("/api/field")
 */
class FieldController extends ApiController
{
    private $fieldRepository;

    public function __construct(FieldRepository $fieldRepository)
    {
        $this->fieldRepository = $fieldRepository;
    }

    /**
     * @Route("/create", name="create_field", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $params = $this->transformJsonBody($request)->request->all();
        $isCorrectParams = isset(
                $params['name'],
                $params['district'],
                $params['fieldShape'],
                $params['fieldArea']
            )
            && !empty($params['name'])
            && !empty($params['district'])
            && !empty($params['fieldShape'])
            && !empty($params['fieldArea']);

        if (!$isCorrectParams) {
            return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params]);
        }

        $field = new Field();
        $field
            ->setName($params['name'])
            ->setDistrict($params['district'])
            ->setFieldShape((int)$params['fieldShape'])
            ->setFieldArea((float)$params['fieldArea'])
            ->setState($params['state'] ?? null)
            ->setTerritory($params['territory'] ?? null)
            ->setGrampancyat($params['grampancyat'] ?? null)
            ->setUser($this->getApiUser());

        $this->fieldRepository->save($field);

        return $this->response(Response::HTTP_CREATED, ['id' => $field->getId()], 'Field successfully created.');
    }

    /**
     * @Route("/update/{id}", name="update_field", methods={"POST"})
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $params = $this->transformJsonBody($request)->request->all();
        $isNotCorrectParams = (isset($params['name']) && empty($params['name'])) ||
            (isset($params['district']) && empty($params['district'])) ||
            (isset($params['fieldShape']) && empty($params['fieldShape'])) ||
            (isset($params['fieldArea']) && empty($params['fieldArea'])) ||
            (isset($params['kml']) && empty($params['kml']));

        if ($isNotCorrectParams || !$field = $this->fieldRepository->find($id)) {
            return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params], 'Fields is required: name, district, fieldShape, fieldArea, kml');
        }

        isset($params['name']) ? $field->setName($params['name']) : null;
        isset($params['district']) ? $field->setDistrict($params['district']) : null;
        isset($params['fieldShape']) ? $field->setFieldShape($params['fieldShape']) : null;
        isset($params['fieldArea']) ? $field->setFieldArea($params['fieldArea']) : null;
        isset($params['state']) ? $field->setState($params['state']) : null;
        isset($params['territory']) ? $field->setTerritory($params['territory']) : null;
        isset($params['grampancyat']) ? $field->setTerritory($params['grampancyat']) : null;
        isset($params['kml']) ? $field->setKml($params['kml']) : null;

        $this->fieldRepository->save($field);

        return $this->response(Response::HTTP_OK, ['id' => $field->getId()], 'Field successfully updated.');
    }

    /**
     * @Route("/{id}", name="get_one_field", methods={"GET"})
     * @param $id
     * @return JsonResponse
     */
    public function getOne($id): JsonResponse
    {
        $field = $this->fieldRepository->find($id);

        if ($field === null) {
            return $this->response(Response::HTTP_BAD_REQUEST, [], 'Field not found.');
        }

        $result = [
            'id' => $field->getId(),
            'name' => $field->getName(),
            'district' => $field->getDistrict(),
            'fieldShape' => $field->getFieldShape(),
            'fieldArea' => $field->getFieldArea(),
            'state' => $field->getState(),
            'territory' => $field->getTerritory(),
            'grampancyat' => $field->getGrampancyat(),
            'user' => $field->getUser() ? $field->getUser()->getId() : 0,
        ];

        return $this->response(Response::HTTP_OK, $result);
    }

    /**
     * @Route("/", name="get_all_field", methods={"GET"})
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $result = array_map(static function (Field $field) {
            return [
                'id' => $field->getId(),
                'name' => $field->getName(),
                'district' => $field->getDistrict(),
                'fieldShape' => $field->getFieldShape(),
                'fieldArea' => $field->getFieldArea(),
                'state' => $field->getState(),
                'territory' => $field->getTerritory(),
                'grampancyat' => $field->getGrampancyat(),
                'user' => $field->getUser() ? $field->getUser()->getId() : 0,
                'kml' => $field->getKml(),
            ];
        }, $this->fieldRepository->findAll());

        return $this->response(Response::HTTP_OK, $result);
    }

    /**
     * @Route("/user/{id}", name="get_all_field_by_user", methods={"GET"})
     * @param $id
     * @return JsonResponse
     */
    public function getAllByUser($id): JsonResponse
    {
        $result = array_map(static function (Field $field) {
            return [
                'id' => $field->getId(),
                'name' => $field->getName(),
                'district' => $field->getDistrict(),
                'fieldShape' => $field->getFieldShape(),
                'fieldArea' => $field->getFieldArea(),
                'state' => $field->getState(),
                'territory' => $field->getTerritory(),
                'grampancyat' => $field->getGrampancyat(),
                'user' => $field->getUser() ? $field->getUser()->getId() : 0,
                'kml' => $field->getKml(),
            ];
        }, $this->fieldRepository->findBy(['user' => $id]));

        return $this->response(Response::HTTP_OK, $result);
    }
}
