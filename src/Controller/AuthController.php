<?php

namespace App\Controller;


use App\Entity\User;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthController extends ApiController
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    public function register(Request $request, UserPasswordEncoderInterface $encoder, ValidatorInterface $validator): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->transformJsonBody($request);
        $login = $request->get('login', '');
        $password = $request->get('password', '');
//        $email = $request->get('email', '');
        $role = $request->get('role', null);

        if (empty($login) || empty($password)) {
            return $this->response(Response::HTTP_BAD_REQUEST, [], "Invalid Username or Password");
        }

        $user = new User($login);
        $user->setPassword($encoder->encodePassword($user, $password));
//        $email ? $user->setEmail($email) : null;
        $role ? $user->setRoles(is_array($role) ? $role : [$role]) : null;

        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            return $this->response(400, [], "Invalid Username or Password");
        }

        if ($this->userRepository->exists($user->getLogin())) {
            return $this->response(400, [], "User exists");
        }

        $em->persist($user);
        $em->flush();

        return $this->response(Response::HTTP_CREATED, [], sprintf('User %s successfully created', $user->getUsername()));
    }

    public function getTokenUser(UserInterface $user = null, JWTTokenManagerInterface $JWTManager = null): JsonResponse
    {
        if ($user !== null) {
            $JWTManager->create($user);
        }

        return $this->response(Response::HTTP_BAD_REQUEST, [], 'Invalid Username or Password');
    }

}