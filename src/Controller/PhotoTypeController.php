<?php

namespace App\Controller;

use App\Entity\CropType;
use App\Entity\PhotoType;
use App\Repository\CropTypeRepository;
use App\Repository\PhotoTypeRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PhotoTypeController
 * @package App\Controller
 * @Route("/api/photo-type")
 */
class PhotoTypeController extends ApiController
{
    private $photoTypeRepository;

    public function __construct(PhotoTypeRepository $photoTypeRepository)
    {
        $this->photoTypeRepository = $photoTypeRepository;
    }

    /**
     * @Route("/", name="get_all_photo_type", methods={"GET"})
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $result = array_map(static function (PhotoType $photoType) {
            return [
                'id' => $photoType->getId(),
                'name' => $photoType->getName(),
            ];
        }, $this->photoTypeRepository->findAll());

        return $this->response(Response::HTTP_OK, $result);
    }
}