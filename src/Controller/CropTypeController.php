<?php

namespace App\Controller;

use App\Entity\CropType;
use App\Repository\CropTypeRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CropTypeController
 * @package App\Controller
 * @Route("/api/crop-type")
 */
class CropTypeController extends ApiController
{
    private $cropTypeRepository;

    public function __construct(CropTypeRepository $cropTypeRepository)
    {
        $this->cropTypeRepository = $cropTypeRepository;
    }

    /**
     * @Route("/", name="get_all_crop_type", methods={"GET"})
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $result = array_map(static function (CropType $cropType) {
            return [
                'id' => $cropType->getId(),
                'name' => $cropType->getName(),
            ];
        }, $this->cropTypeRepository->findAll());

        return $this->response(Response::HTTP_OK, $result);
    }
}