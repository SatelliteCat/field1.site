<?php

namespace App\Controller;

use App\Entity\CropRotation;
use App\Repository\CropRotationRepository;
use App\Repository\CropTypeRepository;
use App\Repository\FieldRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CropRotationController
 * @package App\Controller
 * @Route("/api/crop-rotation")
 */
class CropRotationController extends ApiController
{
    private $cropRotationRepository;
    private $cropTypeRepository;
    private $fieldRepository;

    public function __construct(CropRotationRepository $cropRotationRepository, CropTypeRepository $cropTypeRepository, FieldRepository $fieldRepository)
    {
        $this->cropRotationRepository = $cropRotationRepository;
        $this->cropTypeRepository = $cropTypeRepository;
        $this->fieldRepository = $fieldRepository;
    }

    /**
     * @Route("/create", name="create_crop_rotation", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $params = $this->transformJsonBody($request)->request->all();

        $isCorrectParams = isset(
                $params['type'],
                $params['year'],
                $params['season']
            )
            && (string)$params['year'] !== ''
            && (string)$params['season'] !== '';

        if (!$isCorrectParams) {
            return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params], "Fields is required: type, year, season");
        }

        $cropType = $this->cropTypeRepository->find($params['type']);

        if (!$cropType) {
            return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params], "Type not found");
        }

        $research = new CropRotation();
        $research
            ->setType($cropType)
            ->setYear((int)$params['year'])
            ->setSeason((int)$params['season'])
            ->setCropSort($params['cropSort'] ? (int)$params['cropSort'] : null);

        if (isset($params['fymUsed'])) {
            $research->setFymUsed((bool)$params['fymUsed']);

            if (isset($params['fymQuality'])) {
                if ((bool)$params['fymUsed']) {
                    $research->setFymQuality($params['fymQuality']);
                } else {
                    return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params], "'fymUsed' is false");
                }
            }
        }

        if (isset($params['chemicalFertilizerUsed'])) {
            $research->setChemicalFertilizerUsed((bool)$params['chemicalFertilizerUsed']);

            if (isset($params['chemicalFertilizerQuality'])) {
                if ((bool)$params['chemicalFertilizerUsed']) {
                    $research->setChemicalFertilizerQuality($params['chemicalFertilizerQuality']);
                } else {
                    return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params], "'chemicalFertilizerQuality' is false");
                }
            }
        }

        if (isset($params['dateSeeding'])) {
            try {
                $date = new \DateTime($params['dateSeeding']);
                $research->setDateSeeding($date);
            } catch (\Exception $exception) {
                return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params], "Wrong format 'dateSeeding' (Y-m-d h:i:s)");
            }
        }

        if (isset($params['dateHarvest'])) {
            try {
                $date = new \DateTime($params['dateHarvest']);
                $research->setDateHarvest($date);
            } catch (\Exception $exception) {
                return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params], "Wrong format 'dateHarvest' (Y-m-d h:i:s)");
            }
        }

        if (isset($params['field'])) {
            $field = $this->fieldRepository->find($params['field']);

            if (!$field) {
                return $this->response(Response::HTTP_BAD_REQUEST, ['requestBody' => $params], "Field not found");
            }

            $research->setField($field);
        }

        $this->cropRotationRepository->save($research);

        return $this->response(Response::HTTP_CREATED, ['id' => $research->getId()], 'CropRotation successfully created.');
    }
}