<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

class AuthenticationSuccessListener
{
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $event->setData([
            'code' => $event->getResponse()->getStatusCode(),
            'message' => '',
            'data' => [
                'AccessToken' => $data['token'],
                'RefreshToken' => $data['refreshToken'],
            ],
        ]);
    }
}